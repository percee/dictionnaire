module.exports = function () {
    let config = require('./config.js');
    let Client = require('mariasql');
    let seneca = this;

    let c = new Client({
        host:       config.database.ip,
        port:       config.database.port,
        user:       config.database.user,
        password:   config.database.password,
        db:         config.database.db
    });

    seneca.add({role: 'database', cmd: 'query'}, query);

    function query(args, done) {
        c.query('SELECT mot FROM francais ' + args.query + ' ORDER BY REVERSE(mot)', null, function(err, rows){
            done(null, rows);
        });
    }
};