var config = {};

config.app = {};
config.database = {};

config.app.port = 25000;

config.database.ip = '127.0.0.1';
config.database.port = 3306;
config.database.db = 'dictionnaire';
config.database.user = 'dictionnaire';
config.database.password = 'brasegali';

module.exports = config;