let Seneca = require('seneca');
let Express = require('express');
let Web = require('seneca-web');
let config = require('./config.js');

let config_web = {
    routes: require('./routes.js'),
    adapter: require('seneca-web-adapter-express'),
    context: Express()
};

let seneca = Seneca()
    .use(require('./database.js'))
    .use(require('./dictionnaire.js'))
    .use(Web, config_web)
    .ready(() => {
        let server = seneca.export('web/context')();

        server.listen(config.app.port, () => {
            console.log('server started on port ' + config.app.port);
        })
    });
