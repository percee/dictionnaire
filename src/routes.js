module.exports = [{
    pin: 'role:dictionnaire,cmd:*',
    prefix: '/dictionnaire',
    map: {
        beginWith: {
            GET: true,
            alias: '/dictionnaire/beginWith/:pattern'
        },
        endWith: {
            GET: true,
            alias: '/dictionnaire/endWith/:pattern'
        },
        contains: {
            GET: true,
            alias: '/dictionnaire/contains/:pattern'
        }
    }
}];

// "Bon" tuto: http://jakepruitt.com/2015/01/11/using-seneca-web.html