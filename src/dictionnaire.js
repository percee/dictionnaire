module.exports = function dictionnaire() {
    let seneca = this;

    seneca.add({role: 'dictionnaire', cmd: 'beginWith'}, beginWith);
    seneca.add({role: 'dictionnaire', cmd: 'endWith'}, endWith);
    seneca.add({role: 'dictionnaire', cmd: 'contains'}, contains);

    function beginWith(args, done){
        args = args.args.params;

        seneca.act('role:database,cmd:query',
            {query: 'WHERE mot LIKE \'' + args.pattern + '%\''}, (err, reply) => {
            done(null, reply);
        });
    }

    function endWith(args, done){
        args = args.args.params;

        seneca.act('role:database,cmd:query',
            {query: 'WHERE mot LIKE \'%' + args.pattern + '\''}, (err, reply) => {
                done(null, reply);
            });
    }

    function contains(args, done){
        args = args.args.params;

        seneca.act('role:database,cmd:query',
            {query: 'WHERE mot LIKE \'%' + args.pattern + '%\''}, (err, reply) => {
                done(null, reply);
            });
    }
};